public class Task52A10 {
    public static void main(String[] args) throws Exception {
        Task52A10 task = new Task52A10();
        System.out.println("Total from 1 to 100 is "+task.sumNumbersV1());
        int[] number1={1, 5, 10};
        int[] number2={1,2,3,5,7,9};
        System.out.println("Total Array1: "+task.sumNumbersV2(number1));
        System.out.println("Total Array2: "+task.sumNumbersV2(number2));
        task.printHello(24);
        task.printHello(99);
    }
    public int sumNumbersV1(){
        int sum=0;
        for(int i=1;i<=100;i++){
            sum+=i;
        }
        return sum;
    }
    public int sumNumbersV2(int[] numbers){
        int sum=0;
        for(int i=0;i<numbers.length;i++){
            sum+=numbers[i];
        }
        return sum;
    }
    public int printHello (int number){
        if(number%2==0){
            System.out.println("Day la so chan");
        }
        else{
            System.out.println("Day la so le");
        }
        return 0;
    }
}
