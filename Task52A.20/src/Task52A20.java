import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Task52A20 {
    public boolean isArray(Object variable) {
        return variable instanceof List;
    }

    public int getNthElement(int[] array, int n) {
        if (n < 0 || n >= array.length) {
            return 0;
        }
        return array[n];
    }
    public int[] task3(int[] array){
        Arrays.sort(array);
        return array;
    }
    //task4
    public int task4(int[] array, int value) {
        return Arrays.binarySearch(array, value);
    }
    //task5
    public int[] task5(int[] array1,int[] array2){
        return Arrays.copyOf(array1, array1.length + array2.length);
    }
    //task6
    public Object[] task6(Object[] array) {
        List<Object> filteredList = new ArrayList<>();
        
        for (Object item : array) {
            if (item instanceof Number || item instanceof String) {
                filteredList.add(item);
            }
        }
        
        return filteredList.toArray();
    }
    //task7
    public int[] task7(int[] array, int n) {
        List<Integer> resultList = new ArrayList<>();
        
        for (int value : array) {
            if (value != n) {
                resultList.add(value);
            }
        }
        
        int[] resultArray = new int[resultList.size()];
        for (int i = 0; i < resultList.size(); i++) {
            resultArray[i] = resultList.get(i);
        }
        
        return resultArray;
    }
    //task8
     public int getRandomElement(int[] array) {
        Random random = new Random();
        int randomIndex = random.nextInt(array.length);
        return array[randomIndex];
    }
    //task9
    public int[] createArray(int x, int y) {
        int[] newArray = new int[x];
        Arrays.fill(newArray, y);
        return newArray;
    }
    //task10
    public int[] createConsecutiveArray(int x, int y) {
        int[] newArray = new int[y];
        for (int i = 0; i < y; i++) {
            newArray[i] = x + i;
        }
        return newArray;
    }

    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        // 1
        Task52A20 task = new Task52A20();
        Object inputVar1 = "Devcamp";
        Object inputVar2 = new int[] { 1, 2, 3 };

        System.out.println(task.isArray(inputVar1)); // Output: false
        System.out.println(task.isArray(inputVar2)); // Output: true

        // 2
        int[] inputArray = { 1, 2, 3, 4, 5, 6 };
        int n = 3;
        int n1=6;
        System.out.println(task.getNthElement(inputArray, n)); // Output: 4
        System.out.println(task.getNthElement(inputArray, n1));
        //3
        int[] inputArray3 = {3, 8, 7, 6, 5, -4, -3, 2, 1};
        int[] sortedArray = task.task3(inputArray3);
        for (int i : sortedArray) {
            System.out.print(i+" ");
        }
        System.out.println("\n");
        //4
        int index1 = task.task4(inputArray, 3);
        int index2 = task.task4(inputArray, 7);

        System.out.println("Index of " + 3 + ": " + index1);  // Output: Index of 3: 2
        System.out.println("Index of " + 7 + ": " + index2);  // Output: Index of 7: -1
        //5
        int[] array1 = { 1, 2, 3 };
        int[] array2 = { 4, 5, 6 };

        int[] concatenatedArray = Arrays.copyOf(array1, array1.length + array2.length);
        System.arraycopy(array2, 0, concatenatedArray, array1.length, array2.length);

        System.out.println(Arrays.toString(concatenatedArray));  // Output: [1, 2, 3, 4, 5, 6]
        //6
        Object[] inputArray6 = {Double.NaN, 0, 15, false, -22, "html","develop", 47, null };

        Object[] filteredArray = task.task6(inputArray6);

        System.out.println(Arrays.toString(filteredArray));  
        //7
        int[] array7={2, 5, 9, 6};
        int n7=6;
        System.out.println(Arrays.toString(task.task7(array7, n7)));
        //8
        int[] array8 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        System.out.println(task.getRandomElement(array8));
        System.out.println(task.getRandomElement(array8));
        System.out.println(task.getRandomElement(array8));
        //9
        int x1 = 6, y1 = 0;
        int x2 = 4, y2 = 11;

        int[] array9 = task.createArray(x1, y1);
        int[] array91 = task.createArray(x2, y2);

        System.out.println(Arrays.toString(array9));  
        System.out.println(Arrays.toString(array91));  
        //10
        int x3 = 1, y3 = 4;
        int x4 = -6, y4 = 4;

        int[] array10 = task.createConsecutiveArray(x3, y3);
        int[] array101 = task.createConsecutiveArray(x4, y4);

        System.out.println(Arrays.toString(array10));  
        System.out.println(Arrays.toString(array101));  
    }

}
