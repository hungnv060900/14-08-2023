import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Task52A30 {
    public boolean isString(Object variable) {
        return variable instanceof String;
    }

    public String task2(String input, int number) {
        if (number >= input.length()) {
            return input;
        }
        return input.substring(0, number);
    }

    public String[] task3(String input) {
        return input.split(" ");
    }

    public String task4(String input) {
        String[] words = input.toLowerCase().split(" ");
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            if (i > 0) {
                result.append("-");
            }
            result.append(words[i]);
        }

        return result.toString();
    }
    public String task5(String input) {
        String[] words = input.split(" ");
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            if (i > 0) {
                result.append(" ");
            }
            String word = words[i];
            if (!word.isEmpty()) {
                result.append(Character.toUpperCase(word.charAt(0)));
                result.append(word.substring(1).toLowerCase());
            }
        }

        return result.toString();
    }
    public String capitalizeWords(String input) {
        String[] words = input.split(" ");
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < words.length; i++) {
            if (i > 0) {
                result.append(" ");
            }
            String word = words[i];
            if (!word.isEmpty()) {
                result.append(Character.toUpperCase(word.charAt(0)));
                result.append(word.substring(1).toLowerCase());
            }
        }

        return result.toString();
    }

    public String repeatString(String input, int n) {
        StringBuilder result = new StringBuilder();
        
        for (int i = 0; i < n; i++) {
            if (i > 0) {
                result.append(" ");
            }
            result.append(input);
        }
        
        return result.toString();
    }

    public String[] splitString(String input, int n) {
        List<String> result = new ArrayList<>();
        
        for (int i = 0; i < input.length(); i += n) {
            int endIndex = Math.min(i + n, input.length());
            result.add(input.substring(i, endIndex));
        }
        
        return result.toArray(new String[0]);
    }

    public int countSubstring(String input, String sub) {
        int count = 0;
        int lastIndex = 0;

        while (lastIndex != -1) {
            lastIndex = input.indexOf(sub, lastIndex);

            if (lastIndex != -1) {
                count++;
                lastIndex += sub.length();
            }
        }

        return count;
    }

    public String replaceRight(String input, int n, String replacement) {
        if (n >= input.length()) {
            return replacement;
        }
        return input.substring(0, input.length() - n) + replacement;
    }

    public static void main(String[] args) throws Exception {
        // System.out.println("Hello, World!");
        Task52A30 app = new Task52A30();
        // 1
        System.out.println(app.isString("Devcamp"));
        System.out.println(app.isString(1));
        // 2
        System.out.println(app.task2("Robin Singh", 4));
        // 3
        String input = "Robin Singh";

        String[] result = app.task3(input);

        System.out.println(Arrays.toString(result));
        // 4
        String input4 = "Robin Singh from USA";

        String output = app.task4(input4);

        System.out.println(output);
        //5
        String input1 = "JavaScript Exercises";
        String input2 = "JavaScript exercises";
        String input3 = "JavaScriptExercises";
        String input7 = "JavaScriptExercises";
        String input5 = "JavaScriptExercises";
        String input6 = "JavaScriptExercises";

        System.out.println(app.task5(input1));  
        System.out.println(app.task5(input2));  
        System.out.println(app.task5(input3));  
        System.out.println(app.task5(input7));  
        System.out.println(app.task5(input5));  
        System.out.println(app.task5(input6));
        // Test capitalizeWords
        System.out.println(app.capitalizeWords("js string exercises")); 

        // Test repeatString
        System.out.println(app.repeatString("Ha!", 1));  
        System.out.println(app.repeatString("Ha!", 2));  
        System.out.println(app.repeatString("Ha!", 3));  

        // Test splitString
        System.out.println(Arrays.toString(app.splitString("dcresource", 2)));  
        System.out.println(Arrays.toString(app.splitString("dcresource", 3)));  

        // Test countSubstring
        System.out.println(app.countSubstring("The quick brown fox jumps over the lazy dog", "the")); 

        // Test replaceRight
        System.out.println(app.replaceRight("0000", 2, "123"));  
    }
}
