import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Task52A40 {
     public int[] createArrayInRange(int x, int y) {
        int[] newArray = new int[y - x + 1];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = x + i;
        }
        return newArray;
    }

    public int[] mergeArrays(int[] array1, int[] array2) {
        Set<Integer> mergedSet = new HashSet<>();
        for (int num : array1) {
            mergedSet.add(num);
        }
        for (int num : array2) {
            mergedSet.add(num);
        }
        
        int[] mergedArray = new int[mergedSet.size()];
        int index = 0;
        for (int num : mergedSet) {
            mergedArray[index++] = num;
        }
        
        Arrays.sort(mergedArray);
        return mergedArray;
    }

    public int countOccurrences(int[] array, int n) {
        int count = 0;
        for (int num : array) {
            if (num == n) {
                count++;
            }
        }
        return count;
    }

    public int sumArray(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        return sum;
    }

    public int[] createEvenArray(int[] array) {
        List<Integer> evenList = new ArrayList<>();
        for (int num : array) {
            if (num % 2 == 0) {
                evenList.add(num);
            }
        }
        int[] evenArray = new int[evenList.size()];
        for (int i = 0; i < evenList.size(); i++) {
            evenArray[i] = evenList.get(i);
        }
        return evenArray;
    }

    public int[] addArrays(int[] array1, int[] array2) {
        int[] newArray = new int[Math.min(array1.length, array2.length)];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array1[i] + array2[i];
        }
        return newArray;
    }

    public int[] removeDuplicates(int[] array) {
        Set<Integer> uniqueSet = new HashSet<>();
        for (int num : array) {
            uniqueSet.add(num);
        }
        int[] uniqueArray = new int[uniqueSet.size()];
        int index = 0;
        for (int num : uniqueSet) {
            uniqueArray[index++] = num;
        }
        return uniqueArray;
    }

    public int[] sortDescending(int[] array) {
        Arrays.sort(array);
        int[] descendingArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            descendingArray[i] = array[array.length - i - 1];
        }
        return descendingArray;
    }

    public void swapElements(int[] array, int x, int y) {
        if (x >= 0 && x < array.length && y >= 0 && y < array.length) {
            int temp = array[x];
            array[x] = array[y];
            array[y] = temp;
        }
    }
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        Task52A40 task = new Task52A40();

        // Test createArrayInRange
        System.out.println(Arrays.toString(task.createArrayInRange(4, 7)));  // Output: [4, 5, 6, 7]
        System.out.println(Arrays.toString(task.createArrayInRange(-4, 7)));  // Output: [-4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7]

        // Test mergeArrays
        int[] array2 = {1, 2, 3};
        int[] array3 = {100, 2, 1, 10};
        System.out.println(Arrays.toString(task.mergeArrays(array2, array3)));  // Output: [1, 2, 3, 10, 100]

        // Test countOccurrences
        int[] array4 = {1, 2, 1, 4, 5, 1, 1, 3, 1};
        System.out.println(task.countOccurrences(array4, 1));  // Output: 5

        // Test sumArray
        int[] array5 = {1, 2, 3, 4, 5, 6};
        System.out.println(task.sumArray(array5));  // Output: 21

        // Test createEvenArray
        int[] array6 = {1, 0, 2, 3, 4};
        System.out.println(Arrays.toString(task.createEvenArray(array6)));  // Output: [0, 2, 4]

        // Test addArrays
        int[] array7 = {3, 5, 6, 7, 8, 13};
        int[] array8 = {1, 2, 3};
        System.out.println(Arrays.toString(task.addArrays(array7, array8)));  // Output: [4, 7, 9, 10, 12]
        
        // Test removeDuplicates
        int[] array9 = {1, 3, 1, 4, 2, 5, 6};
        System.out.println(Arrays.toString(task.removeDuplicates(array9)));  // Output: [1, 3, 4, 2, 5, 6]

        // Test sortDescending
        int[] array10 = {10, 20, 30, 40, 52};
        System.out.println(Arrays.toString(task.sortDescending(array10)));  // Output: [52, 40, 30, 20, 10]

        // Test swapElements
        int[] array11 = {1, 2, 3, 1, 5, 1, 4, 6, 3, 4};
        task.swapElements(array11, 3, 4);
        System.out.println(Arrays.toString(array11));
    }
}
