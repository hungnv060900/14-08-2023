public class Task52A50 {
    public int countCharacter(String input, char n) {
        int count = 0;
        for (char c : input.toCharArray()) {
            if (c == n) {
                count++;
            }
        }
        return count;
    }

    public String trimWhiteSpace(String input) {
        return input.trim();
    }

    public String removeSubstring(String input, String sub) {
        return input.replace(sub, "");
    }

    public boolean endsWith(String input, String suffix) {
        return input.endsWith(suffix);
    }

    public boolean compareIgnoreCase(String input1, String input2) {
        return input1.equalsIgnoreCase(input2);
    }

    public boolean isUpperCase(String input, int n) {
        return Character.isUpperCase(input.charAt(n - 1));
    }

    public boolean isLowerCase(String input, int n) {
        return Character.isLowerCase(input.charAt(n - 1));
    }

    public boolean startsWith(String input, String prefix) {
        return input.startsWith(prefix);
    }

    public boolean isEmpty(String input) {
        return input.isEmpty();
    }

    public String reverseString(String input) {
        return new StringBuilder(input).reverse().toString();
    }
    public static void main(String[] args) throws Exception {
       // System.out.println("Hello, World!");
       Task52A50 task = new Task52A50();

        // Test countCharacter
        System.out.println(task.countCharacter("DCresource: JavaScript Exercises", 'e'));  // Output: 5

        // Test trimWhiteSpace
        System.out.println(task.trimWhiteSpace(" dcresource "));  // Output: "dcresource"

        // Test removeSubstring
        System.out.println(task.removeSubstring("dcresource", "resource"));  // Output: "dc"

        // Test endsWith
        System.out.println(task.endsWith("JS PHP PYTHON", "PYTHON"));  // Output: true

        // Test compareIgnoreCase
        System.out.println(task.compareIgnoreCase("abcd", "AbcD"));  // Output: true

        // Test isUpperCase
        System.out.println(task.isUpperCase("Js STRING EXERCISES", 1));  // Output: false
        System.out.println(task.isUpperCase("Js STRING EXERCISES", 2));  // Output: true

        // Test isLowerCase
        System.out.println(task.isLowerCase("Js STRING EXERCISES", 1));  // Output: true
        System.out.println(task.isLowerCase("Js STRING EXERCISES", 2));  // Output: false

        // Test startsWith
        System.out.println(task.startsWith("js string exercises", "js"));  // Output: true

        // Test isEmpty
        System.out.println(task.isEmpty("abc"));  // Output: false
        System.out.println(task.isEmpty(""));     // Output: true

        // Test reverseString
        System.out.println(task.reverseString("AaBbc"));
    }
}
